package alien.optimizers.catalogue;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.catalogue.BookingTable;
import alien.config.ConfigUtils;
import alien.monitoring.Timing;
import alien.optimizers.DBSyncUtils;
import alien.optimizers.Optimizer;

/**
 * Move expired entries from the booking table to the respective `orphan_pfns_[SE#]` table to be reclaimed.
 *
 * @author costing
 * @since 2024-09-10
 */
public class ExpiredBookedLFNs extends Optimizer {

	static final Logger logger = ConfigUtils.getLogger(ExpiredBookedLFNs.class.getCanonicalName());

	/**
	 * Extract PFNs from LFN_BOOKED in batches of 1k entries
	 */
	private static final int BATCH_LENGTH = 1000;

	@Override
	public void run() {
		// default interval between checks of 15 minutes
		this.setSleepPeriod(Duration.ofMinutes(15).toMillis());

		while (true) {
			final boolean updated = DBSyncUtils.updatePeriodic((int) this.getSleepPeriod(), ExpiredBookedLFNs.class.getCanonicalName(), this);

			if (updated) {
				int cnt = 0;
				int done = BATCH_LENGTH;

				try (Timing t = new Timing()) {
					while (done >= BATCH_LENGTH) {
						done = BookingTable.maintenance(BATCH_LENGTH);
						cnt += done;

						DBSyncUtils.setLastActive(ExpiredBookedLFNs.class.getCanonicalName());
					}

					DBSyncUtils.registerLog(ExpiredBookedLFNs.class.getCanonicalName(), "Moved " + cnt + " entries to the orphan tables in " + t);
				}
			}

			try {
				logger.log(Level.INFO, "ExpiredBookedLFNs sleeps " + this.getSleepPeriod());
				sleep(this.getSleepPeriod());
			}
			catch (final InterruptedException e) {
				logger.log(Level.SEVERE, "ExpiredBookedLFNs was interrupted, will exit now", e);
				return;
			}
		}
	}
}
