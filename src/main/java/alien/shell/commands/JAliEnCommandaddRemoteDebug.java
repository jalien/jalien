package alien.shell.commands;

import java.util.List;

import alien.api.Dispatcher;
import alien.api.ServerException;
import alien.api.taskQueue.AddRemoteDebugging;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * @author marta
 */
public class JAliEnCommandaddRemoteDebug extends JAliEnBaseCommand {

	private int instancesToDebug;
	private String siteName;
	private String hostName;
	private long buildTs;

	@Override
	public void run() {
		AddRemoteDebugging instancesAdded;
		try {
			instancesAdded = Dispatcher.execute(new AddRemoteDebugging(instancesToDebug, siteName, hostName, buildTs));
			String additions = instancesAdded.getAdditionSuccess();
			if (additions != null) {
				commander.printOutln("Instances correctly added");
				commander.printOutln(additions);
			} else {
				commander.printOutln("Could not add instances to debug");
			}
		}
		catch (ServerException e) {
			commander.printOutln("Exception executing the command " + e.toString());
		}
	}

	@Override
	public void printHelp() {
		commander.printOutln();
		commander.printOutln(helpUsage("addRemoteDebug:"," add instances of JR to debug"));
		commander.printOutln("        Usage:");
		commander.printOutln("                addRemoteDebug <instances> [-options]");
		commander.printOutln(helpStartOptions());
		commander.printOutln(helpOption("-s", "Sitename (default: any site)"));
		commander.printOutln(helpOption("-host", "Hostname (default: any host)"));
		commander.printOutln(helpOption("-b", "Build Timestamp (long) (default: any ts)"));
		commander.printOutln();
	}

	@Override
	public boolean canRunWithoutArguments() {
		return false;
	}

	/**
	 * Constructor needed for the command factory in commander
	 *
	 * @param commander
	 * @param alArguments
	 */
	public JAliEnCommandaddRemoteDebug(final JAliEnCOMMander commander, final List<String> alArguments) {
		super(commander, alArguments);
		try {
			final OptionParser parser = new OptionParser();

			parser.accepts("s").withRequiredArg();
			parser.accepts("host").withRequiredArg();
			parser.accepts("b").withRequiredArg().ofType(Long.class);

			final OptionSet options = parser.parse(alArguments.toArray(new String[] {}));

			if (options.nonOptionArguments().size() == 0) {
				printHelp();
				return;
			}

			final List<String> nonOptionArguments = optionToString(options.nonOptionArguments());
			instancesToDebug = Integer.valueOf(nonOptionArguments.get(0)).intValue();

			if (options.has("s")) {
				siteName =(String)options.valueOf("s");
			} else {
				siteName = "%%";
			}

			if (options.has("host")) {
				hostName =(String)options.valueOf("host");
			} else {
				hostName = "%%";
			}

			if (options.has("b")) {
				buildTs = ((Long) options.valueOf("b")).longValue();
			} else {
				buildTs = 0l;
			}
		} catch(@SuppressWarnings("unused") NumberFormatException | OptionException e) {
			commander.printOut("Wrong syntax. Please revise the format of command ");
			printHelp();
		}
	}
}
