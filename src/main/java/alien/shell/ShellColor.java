package alien.shell;

/**
 * @author ron
 * @since Oct 27, 2011
 */
public class ShellColor {

	private static final String prefix = "\033[";

	private static final String sep = ";";

	private static final String suffix = "m";

	/**
	 * @return color code
	 */
	public static String reset() {
		return genTag(Style.CLEAR);
	}

	/**
	 * @return color code
	 */
	public static String bold() {
		return genTag(Style.CLEAR);
	}

	/**
	 * @return color code for error Message
	 */
	public static String errorMessage() {
		return boldRed();
	}

	/**
	 * @return color code for info Message
	 */
	public static String infoMessage() {
		return boldBlack();
	}

	/**
	 * @return color code
	 */
	public static String boldRed() {
		return genTag(ForeColor.RED, Style.BOLD);
	}

	/**
	 * @return color code
	 */
	public static String boldWhite() {
		return genTag(ForeColor.WHITE, Style.BOLD);
	}

	/**
	 * @return color code
	 */
	public static String blue() {
		return genTag(ForeColor.BLUE);
	}

	/**
	 * @return color code
	 */
	public static String black() {
		return genTag(ForeColor.BLACK, Style.NONE);
	}

	/**
	 * @return color code
	 */
	public static String boldBlack() {
		return genTag(ForeColor.BLACK, Style.BOLD);
	}

	/**
	 * @return color code
	 */
	public static String jobStateRed() {
		return genTag(ForeColor.RED, BackColor.BLACK);
	}

	/**
	 * @return color code
	 */
	public static String jobStateRedError() {
		return genTag(ForeColor.RED, BackColor.NEUUNFVIERZIG);
	}

	/**
	 * @return color code
	 */
	public static String jobStateBlue() {
		return genTag(ForeColor.BLUE);
	}

	/**
	 * @return color code
	 */
	public static String jobStateBlueError() {
		return genTag(ForeColor.BLUE, BackColor.NEUUNFVIERZIG);
	}

	/**
	 * @return color code
	 */
	public static String jobStateGreen() {
		return genTag(ForeColor.GREEN);
	}

	/**
	 * @return color code
	 */
	public static String jobStateYellow() {
		return genTag(ForeColor.YELLOW);
	}

	/**
	 * @param one
	 * @return the escape sequence to set this style
	 */
	public static String genTag(final ShellCol one) {
		return prefix + one.getCode() + suffix;
	}

	/**
	 * @param one
	 * @param two
	 * @return the escape code to modify two options at the same time
	 */
	public static String genTag(final ShellCol one, final ShellCol two) {
		return prefix + one.getCode() + sep + two.getCode() + suffix;
	}

	// private static String genTag(final ShellCol one, final ShellCol two,
	// final Style three) {
	// return prefix + one.getCode() + sep + two.getCode() + sep +
	// three.getCode() + suffix;
	// }

	/**
	 * Foreground color options
	 */
	public enum ForeColor implements ShellCol {

		/**
		 * Black font
		 */
		BLACK("30"),
		/**
		 * Red font
		 */
		RED("31"),
		/**
		 * Green font
		 */
		GREEN("32"),
		/**
		 * Yellow font
		 */
		YELLOW("33"),
		/**
		 * Blue font
		 */
		BLUE("34"),
		/**
		 * Magenta font
		 */
		MAGENTA("35"),
		/**
		 * Cyan font
		 */
		CYAN("36"),
		/**
		 * White font
		 */
		WHITE("37"),
		/**
		 * No style
		 */
		NONE("");

		private final String code;

		ForeColor(final String code) {
			this.code = code;
		}

		@Override
		public String getCode() {
			return this.code;
		}

	}

	/**
	 * Background color options
	 */
	public enum BackColor implements ShellCol {

		/**
		 * Black background
		 */
		BLACK("40"),
		/**
		 * Red
		 */
		RED("41"),
		/**
		 * Green
		 */
		GREEN("42"),
		/**
		 * Yellow
		 */
		YELLOW("43"),
		/**
		 * Blue
		 */
		BLUE("44"),
		/**
		 * Magenta
		 */
		MAGENTA("45"),
		/**
		 * Cyan
		 */
		CYAN("46"),
		/**
		 * White
		 */
		WHITE("47"),
		/**
		 * Default background color
		 */
		NEUUNFVIERZIG("49"),
		/**
		 * None
		 */
		NONE("");

		private final String code;

		BackColor(final String code) {
			this.code = code;
		}

		@Override
		public String getCode() {
			return this.code;
		}

	}

	/**
	 * Font style
	 */
	public enum Style implements ShellCol {
		/**
		 * Clear modifiers
		 */
		CLEAR("0"),
		/**
		 * Bold
		 */
		BOLD("1"),
		/**
		 * Light
		 */
		LIGHT("1"),
		/**
		 * Dark
		 */
		DARK("2"),
		/**
		 * Underline
		 */
		UNDERLINE("4"),
		/**
		 * Reverse
		 */
		REVERSE("7"),
		/**
		 * Hidden
		 */
		HIDDEN("8"),
		/**
		 * None
		 */
		NONE("");

		private final String code;

		Style(final String code) {
			this.code = code;
		}

		@Override
		public String getCode() {
			return this.code;
		}

	}

	/**
	 * ANSI escape codes
	 */
	public interface ShellCol {
		/**
		 * @return the ANSI code
		 */
		abstract public String getCode();
	}

}
