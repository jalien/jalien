package alien.taskQueue.history;

import java.util.Map;
import java.util.logging.Level;

/**
 * @author Elena Mihailescu
 * @since 2024-08-20
 */
public class JobHistoryFactory extends HistoryFactory {
	private JobHistoryStatistics jobHistoryStatistics;

	/**
	 * Default constructor
	 */
	public JobHistoryFactory() {
		super();

		jobHistoryStatistics = new JobHistoryStatistics("TTLPredictionHistorySite");
		this.addConsumer(jobHistoryStatistics);

		// TODO - Add other JobHistoryConsumers
	}

	/**
	 * @param ttlOptimizationType
	 * @param request
	 * @return the estimated ttl
	 */
	public Long getEstimatedTTL(String ttlOptimizationType, Map<String, Object> request) {

		switch (ttlOptimizationType) {
			case "TTLStatistics": {
				return jobHistoryStatistics.getEstimatedTTL(request);
			}
			default:
				logger.log(Level.SEVERE, "Unexpected ttlOptimizationType: " + ttlOptimizationType);
		}

		return null;
	}

}
