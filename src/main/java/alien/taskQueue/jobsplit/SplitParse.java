package alien.taskQueue.jobsplit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import alien.catalogue.LFN;
import alien.taskQueue.JDL;

/**
 * @author costing
 * @since 2024-07-01
 */
public class SplitParse extends JobSplitter {
	/**
	 * parse to be used on inputdata for grouping, based on split strategy
	 */
	private final String parse;
	/**
	 * list of subjob JDLs
	 */
	private final List<JDL> subjobs = new ArrayList<>();

	/**
	 * @param strategy
	 * @param parse
	 */
	SplitParse(final String strategy, final String parse) {
		this.parse = parse;
		super.strategy = strategy;
	}

	@Override
	List<JDL> splitJobs(final JDL mJDL, final long masterId) throws IOException, IllegalArgumentException {

		masterJdl = mJDL;

		final Map<String, List<LFN>> inputFiles = groupInputFiles(mJDL);
		List<String> tempInput = new ArrayList<>();

		long maxinputsize = 0;
		int maxinputnumber = 0;

		if (mJDL.gets("SplitMaxInputFileNumber") != null) {
			maxinputnumber = Integer.parseInt(mJDL.gets("SplitMaxInputFileNumber"));
		}

		else if (mJDL.gets("SplitMaxInputFileSize") != null) {
			maxinputsize = mJDL.getSize("SplitMaxInputFileSize", 'B', 1_000_000_000);
		}

		long currentSize = 0;

		final JDL baseJDL = prepareSubJobJDL(mJDL, masterId);

		if (mJDL.gets("OrderLFN") != null) {
			sortStrategy = mJDL.gets("OrderLFN");
		}

		for (final Entry<String, List<LFN>> entry : inputFiles.entrySet()) {
			if (sortStrategy != null)
				entry.setValue(sortLFNs(entry.getValue()));

			tempInput = new ArrayList<>();
			if (maxinputsize == 0 && maxinputnumber == 0) {

				for (final LFN file : entry.getValue()) {
					tempInput.add(file.lfn);
				}
				addSubjob(baseJDL, tempInput, currentSize);
			}
			else {
				if (maxinputnumber != 0) {

					int currentnumber = 0;
					int numberGroups = (int) Math.ceil((double) entry.getValue().size() / maxinputnumber);
					if (numberGroups == 0)
						numberGroups = 1;

					int tmpMax = entry.getValue().size() / numberGroups;

					for (final LFN file : entry.getValue()) {

						if (currentnumber >= tmpMax && currentnumber != 0) {
							numberGroups--;
							tmpMax = ((entry.getValue().size() - currentnumber) / numberGroups) + currentnumber;
							addSubjob(baseJDL, tempInput, currentSize);
							tempInput = new ArrayList<>();

						}
						tempInput.add(file.lfn);
						currentnumber++;
					}
				}

				else {

					for (final LFN file : entry.getValue()) {
						if (maxinputsize < currentSize + file.getSize() && !tempInput.isEmpty()) {
							addSubjob(baseJDL, tempInput, currentSize);
							tempInput = new ArrayList<>();
							currentSize = 0;
						}

						currentSize += file.getSize();
						tempInput.add(file.lfn);

					}

				}
				if (!tempInput.isEmpty()) {
					addSubjob(baseJDL, tempInput, currentSize);
				}
			}
		}

		return subjobs;
	}

	/**
	 * Complete subjob JDL preparation and add to list for inserting
	 *
	 * @param baseJDL base JDL for subjobs prepared before splitting
	 * @param tempInput List of inputdata files for subjob
	 * @param currentSize size of inputdata files if applicable
	 */
	private void addSubjob(final JDL baseJDL, final List<String> tempInput, final long currentSize) throws IllegalArgumentException {
		final JDL tmpJdl = changeFilePattern(baseJDL, tempInput);
		tmpJdl.addRequirement(setSizeRequirements(currentSize));
		tmpJdl.set("InputData", tempInput);
		subjobs.add(tmpJdl);
	}

	/**
	 * Group inputdata into baskets based on filename after parsing
	 *
	 * @param mJDL masterjob JDL
	 * @return Map of baskets and list of inputdata files
	 * @throws IOException
	 */
	public Map<String, List<LFN>> groupInputFiles(final JDL mJDL) throws IOException {
		final Map<String, List<LFN>> groupedFiles = new HashMap<>();

		final Collection<LFN> inputData = mJDL.getInputLFNs();

		checkNoDownload();

		if (inputData.isEmpty()) {
			throw new IOException("Could not find inputdata");
		}

		for (final LFN file : inputData) {

			final String fileS = file.getCanonicalName();
			final String newFile = fileS.replaceAll(parse, "");

			if (!groupedFiles.containsKey(newFile)) {
				groupedFiles.put(newFile, new ArrayList<>());
			}

			file.lfn = getFinalLfnString(fileS);

			groupedFiles.get(newFile).add(file);
		}

		return groupedFiles;
	}
}
